"""
Compare several namelist files
"""
import os
import lib as l
import numpy as np


# --- Settings:
referenceModel = 'DIIID'
models = ('DIIID1', 'DIIID2', 'DIIID3', 'DIIID4')
far3dMain = '/home/joserueda/FAR3d'


# --- Comparison:
model0NM = l.readNamelist(os.path.join(
    far3dMain, 'Models', referenceModel, 'Input_Model'))
nm = []
differences = []
# get the differences
for im, model in enumerate(models):
    nm.append(l.readNamelist(os.path.join(
        far3dMain, 'Models', models[im], 'Input_Model')))
    for key in nm[im].keys():
        try:
            if model0NM[key] != nm[im][key]:
                differences.append(key)
        except ValueError:
            if not np.all(model0NM[key] == nm[im][key]):
                differences.append(key)
# get the different values
print(referenceModel, models)
for key in differences:
    line = [nm[i][key] for i in range(len(nm))]
    print(key, model0NM[key], line)
