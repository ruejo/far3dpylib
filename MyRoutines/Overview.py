"""
Basic overview of a FAR3d simulation output
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import lib as l

# --- Settings
far3dMain = '/home/joserueda/FAR3d'
model = 'DIIID'

# --- Load the farprt main output file
farfile = os.path.join(far3dMain, 'Models', model, 'farprt')
far = l.Farprt(farfile)
far.readEnergyBlock()

# --- Load the mode information
modes = l.Modes(model)

# --- Plot something
far.plotEnergy()  # Notice, this is not properly normalised so the y axis has not a lot of meaning
modes.plotRho(var_name='pr', m=1)  # Plot the mode amplitude of the pressure, of m=1,n, R or  I... are optional arguments
