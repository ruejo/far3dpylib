import os
import sys

def paths_of_the_suite(machine='pc'):
    """
    Add to the path all the necessary folders for the library to run.

    Jose Rueda Rueda: jose.rueda@ipp.mpg.de

    @param machine: Machine where we are working
    """
    # --- Section 0: Name of the auxiliary folders (located at home directory)
    HOME_DIR = os.getenv("HOME")
    far3D_DIR = os.getcwd()
    lib_LIBs = {
        'Base': far3D_DIR,
    }

    # -- Machine dependent folders:
    Machine_libs = {
        'AUG': {
            'AUG_Python': '/afs/ipp/aug/ads-diags/common/python/lib',
            # 'Suite_AUG': os.path.join(SUITE_DIR, 'Lib/LibData/AUG')
        }
    }

    # --- Section 1: Add folders to path
    # Suite directories:
    for lib in lib_LIBs.keys():
        sys.path.extend([os.path.join(far3D_DIR, lib_LIBs[lib])])
    # Machine dependent paths:
    if machine in Machine_libs:
        for lib in Machine_libs[machine].keys():
            sys.path.extend([os.path.join(far3D_DIR, Machine_libs[machine][lib])])

    # Check the cluster where we are working
    if machine == 'AUG':
        cluster = os.getenv('HOST')
        if cluster[:4] != 'toki':
            print('We are not in toki')
            print('The suite has not been tested outside toki')
            print('Things can go wrong!')
    else:
        print('Not recognised tokamak environment')
        print('We assume you are in your personal PC')
        print('You will not have access to databases')


if __name__ == "__main__":

    if os.path.isdir('/afs/ipp-garching.mpg.de'):
        machine = 'AUG'
    else:
        machine = 'Generic'

    paths_of_the_suite(machine)
