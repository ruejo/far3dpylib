from lib._farprt import Farprt
from lib._modes import Modes
from lib.namelist import readNamelist
import lib.plotting as plt
import lib.profiles as profiles

try:
    plt.plotSettings()
except:
    print('It was not possible to initialise the plotting settings')
